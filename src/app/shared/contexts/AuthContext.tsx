import React from "react";
import { authProvider } from "../services/auth";
import { Navigate, useLocation } from "react-router-dom";
import { IAuthContextType } from "../models/IAuthContextType";

export function useAuth() {
  return React.useContext(AuthContext);
}

export function AuthProvider({ children }: { children: React.ReactNode }) {
  let [user, setUser] = React.useState<any>(null);

  let signup = (firstName: string,
    lastName: string,
    email: string,
    password: string,
    callback: VoidFunction,
    callbackError: Function) => {
    return authProvider.signup(firstName, lastName, email, password, () => {
      callback();
    }, (error: Error) => {
      callbackError(error)
    });
  }

  let signin = (email: string,
    password: string,
    callback: VoidFunction,
    callbackError: Function) => {
    return authProvider.signin(email, password, () => {
      const user = sessionStorage.getItem("jwt_token") ? sessionStorage.getItem("jwt_token") : ""
      setUser(user);
      callback();
    }, (error: Error) => {
      callbackError(error)
    });
  };

  let signout = (callback: VoidFunction) => {
    return authProvider.signout(() => {
      setUser(null);
      sessionStorage.removeItem("token");
      localStorage.removeItem("user");
      callback();
    });
  };

  let value = { user, signup, signin, signout };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export function RequireAuth({ children }: { children: JSX.Element }) {
  let location = useLocation();
  const token = sessionStorage.getItem("token")
  if (!token) {
    return <Navigate to="/" state={{ from: location }} replace />;
  }
  return children;
}

export const AuthContext = React.createContext<IAuthContextType>({} as IAuthContextType);
