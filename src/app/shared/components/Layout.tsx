import { Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { MyRoutes } from "../../routes/routes";

import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export const Layout = () => {
  let navigate = useNavigate();
  const logged = sessionStorage.getItem("token")

  function handlerLogout() {
    localStorage.removeItem("user")
    sessionStorage.removeItem("token")
    navigate("/")
  }

  return (
    <>
      <nav className="navbar bg-dark border-bottom border-body navbar-expand-lg bg-body-tertiary" data-bs-theme="dark">
        <div className="container-fluid">
          <div className="navbar-brand">
            <Link style={{ textDecoration: 'none', color: 'white' }} to="/home">Controle de gastos</Link>
          </div>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <div className="nav-link active" aria-current="page">
                  <Link style={{ textDecoration: 'none', color: 'white' }} to="/revenue">Revenue</Link>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <Link style={{ textDecoration: 'none', color: 'white' }} to="/spending">spending</Link>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <Link style={{ textDecoration: 'none', color: 'white' }} to="/user">users</Link>
                </div>
              </li>
              <li className="nav-item">
              </li>
            </ul>
            <form className="d-flex" role="search">
              {logged ? <Button variant="danger" onClick={() => handlerLogout()}>Sair</Button> : ' '}
            </form>
          </div>
        </div>
      </nav>
      <div >
        <div className="container mt-5">
          <Container>
            <MyRoutes />
          </Container>
        </div>
      </div>
    </>
  );
}