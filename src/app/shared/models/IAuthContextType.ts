export interface IAuthContextType {
  user: string | null;
  signup: (firstName: string, lastName: string, email: string, password: string, callback: VoidFunction, callbackError: Function) => void;
  signin: (email: string, password: string, callback: VoidFunction, callbackError: Function) => void;
  signout: (callback: VoidFunction) => void;
}