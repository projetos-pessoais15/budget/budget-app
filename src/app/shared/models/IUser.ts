import { IRole } from "./IRole"

export interface IUser {
  id?: string
  firstName: string
  lastName: string
  email: string
  password: string
  roles: IRole[]
}