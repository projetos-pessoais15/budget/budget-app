import { ISort } from "./ISort";

export interface IPageable {
  offSet: number,
  pageNumber: 0,
  pageSize: number,
  paged: true,
  sort: ISort,
  unpaged: boolean
}