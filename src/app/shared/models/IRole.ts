export interface IRole {
  id: number
  authority?: string
}

export interface IRolePost {
  id: number
}