import { IPageable } from './IPageable';
import { ISort } from './ISort';

export interface IResponse {
  timeStamp: Date;
  statusCode: number;
  status: string;
  reason: string;
  message: string;
  developMessage: string;
  data: [];
  list: [];
}