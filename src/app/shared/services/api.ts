import axios from 'axios';

const api = axios.create({});

const tokenStr = localStorage.getItem("token");

let token = null;
if (tokenStr) {
  token = JSON.parse(tokenStr);
}

api.defaults.baseURL = 'http://localhost:8080/budget-api'
api.defaults.headers.common['Authorization'] = token;
api.defaults.headers.common['Content-type'] = 'application/json';

export default api;



