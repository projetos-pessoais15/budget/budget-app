import { IUser } from "../models/IUser";
import api from "./api";

export async function getInfoUser() {
  return await api.get(`/user/info`
  ).then((response) => {
    const { data } = response
    return data
  }).catch((error) => {
    return error
  });
}

export async function findAllUsers() {
  return api.get(`/user`)
    .then((response) => {
      const { data } = response
      return data
    }).catch((error) => {
      return error
    });
}

export async function findUserById(id: string | undefined) {
  return await api.get(`/user/${id}`)
    .then((response) => {
      const { data } = response
      return data
    }).catch((error) => {
      return error
    });
}

export async function deleteUserById(id: string) {
  return await api.delete(`/user/${id}`)
    .then((response) => {
      const { data } = response
      return data
    }).catch((error) => {
      return error
    });
}

export async function saveUser() {
  return await api.post(`/user`)
    .then((response) => {
      const { data } = response
      return data
    }).catch((error) => {
      return error
    });
}

export async function updateUser(user: IUser) {
  return await api.put(`/user`, user)
    .then((response) => {
      const { data } = response
      return data
    }).catch((error) => {
      return error
    });
}
