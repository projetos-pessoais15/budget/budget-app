import { IRolePost } from './../models/IRole';
import api from './api';

export const authProvider = {
  isAuthenticated: false,
  signup(firstName: string, lastName: string, email: string, password: string, callback: VoidFunction, callbackError: Function) {
    authProvider.isAuthenticated = false;
    return api.post(`/auth/register`, { firstName, lastName, email, password, roles: [{ id: 2 }] }
    ).then((response) => {
      callback()
      return response;
    }, (error) => {
      callbackError(error.response.data)
    });
  },
  signin(email: string, password: string, callback: VoidFunction, callbackError: Function) {
    authProvider.isAuthenticated = true;
    return api.post(`/auth/login`, { email, password, }
    ).then((response) => {
      if (response.data.jwt_token) {
        authProvider.isAuthenticated = true;
        localStorage.setItem("user", JSON.stringify(response.data.user))
        sessionStorage.setItem("token", JSON.stringify(response.data.jwt_token));
      }
      callback()
      return response;
    }, (error) => {
      callbackError(error.response.data)
    });
  },
  signout(callback: VoidFunction) {
    authProvider.isAuthenticated = false;
    localStorage.removeItem("user")
    sessionStorage.removeItem("token")
    callback()
  },
};
