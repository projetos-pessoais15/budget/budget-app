import { Route, Routes } from "react-router-dom";
import Home from "../pages/home/Home";
import Register from "../pages/register/Register";
import AdditionalIcome from "../pages/additional-icome/AdditionalIcome";
import Revenue from "../pages/revenue/Revenue";
import Spending from "../pages/spending/Spending";
import User from "../pages/user/User";
import { AuthProvider, RequireAuth } from "../shared/contexts/AuthContext";
import { Login } from "../pages/login/Login";
import { UserInfo } from "../pages/user/UserInfo";
import { UserEdit } from "../pages/user/UserEdit";

export const MyRoutes = () => {
  const token = sessionStorage.getItem("token")


  return (
    <AuthProvider>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/register/:newUser" element={<Register />} />
        <Route path="/register/edit/:id" element={<RequireAuth><Register /></RequireAuth>} />
        <Route path="/home" element={<RequireAuth><Home /></RequireAuth>} />
        <Route path="/additional-income" element={<RequireAuth><AdditionalIcome /></RequireAuth>} />
        <Route path="/revenue" element={<RequireAuth><Revenue /></RequireAuth>} />
        <Route path="/spending" element={<RequireAuth><Spending /></RequireAuth>} />
        <Route path="/user" element={<RequireAuth><User /></RequireAuth>} />
        <Route path="/user/:id" element={<RequireAuth><UserInfo /></RequireAuth>} />
      </Routes>
    </AuthProvider>
  );
}