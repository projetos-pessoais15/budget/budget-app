import { BrowserRouter, useNavigate } from "react-router-dom";
import { Layout } from "./shared/components/Layout";

export const App = () => {
  return (
    <BrowserRouter>
      <Layout />
    </BrowserRouter >
  );
}

