import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { IUser } from "../../shared/models/IUser";
import { deleteUserById, findAllUsers } from "../../shared/services/user";
import { Link, useNavigate } from "react-router-dom";

const User = () => {
  const [message, setMessage] = useState({
    error: "",
    path: "",
    status: "",
    timestamp: ""
  })
  const [users, setUsers] = useState([]);
  let navigate = useNavigate();

  useEffect(() => {
    loadUsers()
  }, []);

  const loadUsers = async () => {
    const list = findAllUsers()
    setUsers(await list)
  };


  const handlerDeleteUser = async (id: string) => {
    const data = await deleteUserById(id)
    console.log('data', data.statusCode)
    if (data.statusCode === 200) {
      window.location.reload();
    }
  };

  return (
    <>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Email</th>
            <th>Roles</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user: IUser) => {
            return (
              <tr key={user.id}>
                <td>{user.firstName}{' '}{user.lastName}</td>
                <td>{user.email}</td>
                <td >{user.roles.map((role) => {
                  return <span key={role.id}>{role.authority}{', '}</span>
                })}</td>
                <td>
                  <div className="btn-group btn-group-sm" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-warning" >
                      <Link style={{ textDecoration: 'none', color: 'black' }} to={`/register/edit/${user.id}`}>Atualizar</Link>
                    </button>
                    <button type="button" className="btn btn-danger" onClick={() => { handlerDeleteUser(user.id ? user.id : '') }}>Excluir</button>
                  </div>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>
      <div className="align-bottom">
        <button type="button" className="btn btn-success">
          <Link style={{ textDecoration: 'none', color: 'white' }} to={`/register/${'newUser'}`}>Novo usuário</Link>
        </button>{' '}
      </div>

      <div>
        {message ? <p>{message.error}</p> : null}
        {message ? <p>{message.path}</p> : null}
        {message ? <p>{message.status}</p> : null}
        {message ? <p>{message.timestamp}</p> : null}
      </div>

    </>
  );
};

export default User;