import { useParams } from "react-router-dom";
import { IUser } from "../../shared/models/IUser";

interface Props {
  user?: IUser;
  onChange?: (fieldName: string, value: string) => void;
  onSave?: () => void;
}

export const UserEdit: React.FunctionComponent<Props> = (props?: Props) => {
  const { id } = useParams<{ id: string }>();

  return (
    <>
      <p>Editar usuário</p>
    </>
  )
}