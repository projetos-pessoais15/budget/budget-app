import { useLocation, useNavigate } from "react-router-dom";
import React, { useState } from "react";
import { useAuth } from "../../shared/contexts/AuthContext";
import { Alert, Button, Form } from "react-bootstrap";
import { Formik } from "formik";

import * as Yup from 'yup';
import { Link } from "react-router-dom";

export const Login = () => {
  const [message, setMessage] = useState("")
  let navigate = useNavigate();
  let location = useLocation();
  let auth = useAuth();

  let from = location.state?.from?.pathname || "/home";

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    let formData = new FormData(event.currentTarget);
    let email = formData.get("email") as string;
    let password = formData.get("password") as string;

    auth.signin(email, password, () => {
      navigate(from, { replace: true });
    }, (error: any) => {
      setMessage(error.message)
    });
  }

  const SigninSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Required'),
  });

  return (
    <>
      <div className="mt-5">
        <h1>Login</h1>
        <Formik
          initialValues={{
            email: '',
            password: ''
          }}
          validationSchema={SigninSchema}
          onSubmit={values => {
            console.log(values);
            { handleSubmit }
          }}
        >
          {({ errors, touched }) => (
            <Form onSubmit={handleSubmit}>
              <div className="mt-5">
                <Form.Group className="mb-3" controlId="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" placeholder="Digite seu email" name="email" />
                  {errors.email && touched.email ? (
                    <div>{errors.email}</div>
                  ) : null}
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                  <Form.Label>Senha</Form.Label>
                  <Form.Control type="password" placeholder="Digite sua senha" name="password" />
                  {errors.password && touched.password ? (
                    <div>{errors.password}</div>
                  ) : null}
                </Form.Group>
                {message ?
                  <Alert key={"danger"} variant={"danger"}>
                    {message}
                  </Alert> : null}

                <div>
                  <Button id="submit" type="submit" variant="primary">Entrar</Button>{' '}
                  <Button type="submit" id="register" variant="light"><Link to="/register">Novo Usuário</Link></Button>{' '}
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div >
    </>
  );
}

