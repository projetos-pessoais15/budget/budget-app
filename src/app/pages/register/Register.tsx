import { useAuth } from "../../shared/contexts/AuthContext";
import Col from 'react-bootstrap/Col';
import { useEffect, useState } from "react";
import { Button, Form, Row } from "react-bootstrap";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import * as formik from 'formik';
import * as yup from 'yup';
import { IRegister } from "../../shared/models/IRegister";
import { findUserById, updateUser } from "../../shared/services/user";
import { IUser } from "../../shared/models/IUser";

const Register = () => {
  const { Formik } = formik;
  const { id } = useParams<{ id: string }>();
  const { newUser } = useParams<{ newUser: string }>();
  const [message, setMessage] = useState("")
  const [user, setUser] = useState({} as IUser)
  const [validated, setValidated] = useState(false);

  let auth = useAuth();
  let location = useLocation();
  let navigate = useNavigate();

  useEffect(() => {
    if (id) {
      getUser()
    }
  }, [])

  const getUser = async () => {
    const data = await findUserById(id)
    setUser(data.object)
  }

  const getRoles = async () => {
    const data = await findUserById(id)
    setUser(data.object)
  }

  let from = ""
  const handleSubmit = (object: IRegister) => {
    if (newUser) {
      from = location.state?.from?.pathname || "/user";
      handleUpdate(object)
    } else {
      from = location.state?.from?.pathname || "/";
      handleRegister(object)
    }
  }

  const handleUpdate = async (object: IRegister) => {
    let userUpdated = {} as IUser
    userUpdated = {
      id: user.id,
      email: object.email,
      firstName: object.firstName,
      lastName: object.lastName,
      password: object.password,
      roles: user.roles
    }

    const status = await updateUser(userUpdated)
    navigate(from, { replace: true });
    console.log('STATUS', status)
  };

  const handleRegister = (object: IRegister) => {
    let firstName = object.firstName
    let lastName = object.lastName
    let email = object.email
    let password = object.password

    auth.signup(firstName, lastName, email, password, () => {
      navigate(from, { replace: true });
    }, ((error: any) => {
      setMessage(error.message)
    }))
  };

  let initialValues = {} as IRegister
  if (user) {
    initialValues = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: '',
      confirmPassword: ''
    }
  } else {
    initialValues = {
      id: '',
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: ''
    }
  }

  const validationSchema = yup.object().shape({
    firstName: yup.string()
      .required('Nome é obrigatório!')
      .min(4, 'Seu nome precisa ter entre 6 e 20 caracteres')
      .max(20, 'Seu nome é maior do que 20 caracteres'),
    lastName: yup.string()
      .required('Sobrenome é obrigatório!')
      .min(4, 'Seu sobrenome precisa ter entre 6 e 20 caracteres')
      .max(20, 'Seu sobrenome é maior do que 20 caracteres'),
    email: yup.string().required('Email é obrigatório')
      .email('Email é inválido'),
    password: yup.string()
      .required('Senha é obrigatória!')
      .min(4, 'Sua senha precisa ter entre 5 e 20 caracteres')
      .max(20, 'Sua senha é maior do que 20 caracteres'),
    confirmPassword: yup.string()
      .required('Você precisa confirmar a sua senha')
      .oneOf([yup.ref('password')], 'Suas senhas não batem'),
  });

  return (
    <>
      <div className="mt-5">
        <h1>Novo usuário</h1>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values) => {
            console.log(values)
            { handleSubmit(values) }
          }}
        >
          {({ handleSubmit, handleChange, touched, errors }) => (
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
              <div className="mt-5">

                <Row className="mb-3">
                  <Form.Group as={Col} md="6" controlId="id">
                    <Form.Label>ID</Form.Label>
                    <Form.Control
                      disabled={true}
                      defaultValue={initialValues.id}
                      type="text"
                      name="id"
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Row>

                <Row className="mb-3">
                  <Form.Group as={Col} md="6" controlId="firstName">
                    <Form.Label>Nome</Form.Label>
                    <Form.Control
                      defaultValue={initialValues.firstName}
                      type="text"
                      name="firstName"
                      placeholder="Digite seu nome"
                      onChange={handleChange}
                      isValid={touched.firstName && !errors.firstName}
                      isInvalid={!!touched.firstName && !!errors.firstName}
                      required
                    />
                    <Form.Control.Feedback>Ótimo!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">{errors.firstName}</Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group as={Col} md="6" controlId="lastName">
                    <Form.Label>Sobrenome</Form.Label>
                    <Form.Control
                      defaultValue={initialValues.lastName}
                      type="text"
                      name="lastName"
                      placeholder="Digite seu sobrenome"
                      onChange={handleChange}
                      isValid={touched.lastName && !errors.lastName}
                      isInvalid={!!touched.lastName && !!errors.lastName}
                      required
                    />
                    <Form.Control.Feedback>Ótimo!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">{errors.lastName}</Form.Control.Feedback>
                  </Form.Group>
                </Row>

                <Row className="mb-3">
                  <Form.Group as={Col} md="12" controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      defaultValue={initialValues.email}
                      type="email"
                      name="email"
                      placeholder="Digite seu email"
                      onChange={handleChange}
                      isValid={touched.email && !errors.email}
                      isInvalid={!!touched.email && !!errors.email}
                      required
                    />
                    <Form.Control.Feedback>Ótimo!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">{errors.email}</Form.Control.Feedback>
                  </Form.Group>
                </Row>

                <Row className="mb-3">
                  <Form.Group as={Col} md="6" controlId="password">
                    <Form.Label>Senha</Form.Label>
                    <Form.Control
                      defaultValue={initialValues.password}
                      type="password"
                      name="password"
                      placeholder="Digite sua senha"
                      onChange={handleChange}
                      isValid={touched.password && !errors.password}
                      isInvalid={!!touched.password && !!errors.password}
                      required
                    />
                    <Form.Control.Feedback>Ótimo!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">{errors.password}</Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group as={Col} md="6" controlId="confirmPassword">
                    <Form.Label>Confirme a senha</Form.Label>
                    <Form.Control
                      defaultValue={initialValues.confirmPassword}
                      type="password"
                      name="confirmPassword"
                      placeholder="Confirme sua senha"
                      onChange={handleChange}
                      isValid={touched.confirmPassword && !errors.confirmPassword}
                      isInvalid={!!touched.confirmPassword && !!errors.confirmPassword}
                      required
                    />
                    <Form.Control.Feedback>Ótimo!</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">{errors.confirmPassword}</Form.Control.Feedback>
                  </Form.Group>
                </Row>

                <div>
                  <Button id="submit" type="submit" variant="success">Salvar</Button> {' '}
                  <Button id="reset" type="reset" variant="warning">Limpar</Button>
                </div>
              </div>

              {message ? <div>{message}</div> : null}
            </Form>
          )}
        </Formik>
      </div>
    </>
  );
};

export default Register;
